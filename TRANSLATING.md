# How to add a translation (the simple way)

 * open the [file](https://gitlab.com/marcjeanmougin/inkscape-carto-ext/-/raw/master/locale/hatches_grains.pot) in `locale/hatches_grains.pot` with a translation software
   such as poedit (More prceisely, open poedit, then select "Create a new translation from the pot file" with the pot file in the locale folder).
 * translate the strings, save.
 * [Open a new issue](https://gitlab.com/marcjeanmougin/inkscape-carto-ext/-/issues/new) and attach your saved .po file to the issue.



# How to add a translation (the standard and prefered way)

 * Create a language translation folder with your language [ISO code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) in the `locale/` folder
 * Create a subfolder `LC_MESSAGES` in it
 * Copy the `locale/hatches_grains.pot` file into `hatches_grains.po` into your language translation folder
 * Edit it with poedit, or any other standard translation tool (this is the
   translation part).
 * Generate the .mo file (this is done automatically by poedit when saving).
   Put that file in the `locale/<code>/LC_MESSAGES/` folder.
 * If you are confused, look at how the fr/ (French) translation folder is
   structured and try to mimic it)
 * Done ! Note that if people modify the inx file to add or change strings, you will just need to merge the newer pot file and update only the modified translations.

Then feel free to make a merge request with your new translation :)


# How to regenerate the pot file if you add or change strings

First, get the inx.its file on the [inkscape repo](https://gitlab.com/inkscape/inkscape/-/raw/master/po/its/inx.its).  Then run:


```bash
xgettext hatches_grains.inx --its=inx.its -o locale/hatches_grains.pot
```

Then update languages po file either with poedit (or your favourite tool) or with msgmerge directly. 

This will be done automatically once I setup CI so you don't have to worry about it too much.


